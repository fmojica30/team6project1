from flask import Flask, render_template, request, redirect, url_for, jsonify
import random

app = Flask(__name__)

@app.route('/book/JSON')
def bookJSON():
#<your code> Nothing needs to be added here.
    print()
@app.route('/')
@app.route('/drugs/')
def showDrugs():
    return render_template('drugInfo.html',)

@app.route('/cannabis', methods=['GET', 'POST'])
def Cannabis():
    render_template('Cannabis.html')

if __name__ == '__main__':
    app.debug = True
    app.run()
